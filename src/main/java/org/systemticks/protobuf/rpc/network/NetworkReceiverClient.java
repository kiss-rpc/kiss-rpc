package org.systemticks.protobuf.rpc.network;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.systemticks.protobuf.rpc.data.Header;
import org.systemticks.protobuf.rpc.data.MsgQueueElement;

public class NetworkReceiverClient extends NetworkReceiver {

	@Override
	public void processIncomingMessage(Header header, byte[] payload) throws InterruptedException {

		String key = header.getSerivce()+"."+header.getClientToken();

		System.out.println("MessageBroker: Processing next message "+key);
		
		if (!messageHandler.containsKey(key)) {
			System.out.println("Unknown Service: " + key);
		} else {
			messageHandler.get(key).put(new MsgQueueElement(header, payload));
		}
	}
	
	public BlockingQueue<MsgQueueElement> registerMessageHandler(String service, String token) {
		String key = service+"."+token;
		if (!messageHandler.containsKey(key)) {
			messageHandler.put(key, new ArrayBlockingQueue<>(20));
		}
		return messageHandler.get(key);
	}
	
	
}
