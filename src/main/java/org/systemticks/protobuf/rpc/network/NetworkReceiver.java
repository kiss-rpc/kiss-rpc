package org.systemticks.protobuf.rpc.network;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.systemticks.protobuf.rpc.data.Header;
import org.systemticks.protobuf.rpc.data.MsgQueueElement;

public class NetworkReceiver implements Runnable {

	protected Map<String, BlockingQueue<MsgQueueElement>> messageHandler = new HashMap<>();
	DataInputStream inStream;
	
	public NetworkReceiver() {
	}
	
	public void bindStream(DataInputStream _inStream) {
		inStream = _inStream;		
	}

	public BlockingQueue<MsgQueueElement> registerMessageHandler(String service, String token) {
		String key = service+"."+token;
		if (!messageHandler.containsKey(key)) {
			messageHandler.put(key, new ArrayBlockingQueue<>(20));
		}
		return messageHandler.get(key);
	}

	public BlockingQueue<MsgQueueElement> registerMessageHandler(String service) {
		if (!messageHandler.containsKey(service)) {
			messageHandler.put(service, new ArrayBlockingQueue<>(20));
		}
		return messageHandler.get(service);
	}

	public void processIncomingMessage(Header header, byte[] payload) throws InterruptedException {

		String key = header.getSerivce();
		System.out.println("MessageBroker: Processing next message "+key);
		
		if (!messageHandler.containsKey(key)) {
			System.out.println("Unknown Service: " + key);
		} else {
			messageHandler.get(key).put(new MsgQueueElement(header, payload));
		}
	}

	@Override
	public void run() {
		boolean connectionValid = true;
		
		while (connectionValid) {
			try {								
				Header header = Header.parseDelimitedFrom(inStream);			
				byte[] payload = new byte[header.getPayloadSize()];
				inStream.read(payload);
				processIncomingMessage(header, payload);
				
			} catch (IOException | InterruptedException e) {
				connectionValid = false;
				e.printStackTrace();
			}
		}
	}

}
