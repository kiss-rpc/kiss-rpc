package org.systemticks.protobuf.rpc.network;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;

import org.systemticks.protobuf.rpc.data.MsgQueueElement;

public class NetworkSender implements Runnable {

	private BlockingQueue<MsgQueueElement> outGoingMessages;
	private DataOutputStream outStream;

	public NetworkSender(BlockingQueue<MsgQueueElement> blockingQueue) {
		outGoingMessages = blockingQueue;
	}

	public void bindStream(DataOutputStream _outStream) {
		outStream = _outStream;
	}

	@Override
	public void run() {

		while (true) {
			System.out.println("MessageSender: Sending back");

			try {
				MsgQueueElement msg = outGoingMessages.take();
				msg.getHeader().writeDelimitedTo(outStream);				
				outStream.write(msg.getMessage());						
				outStream.flush();		
			} catch (InterruptedException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
