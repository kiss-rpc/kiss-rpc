package org.systemticks.protobuf.rpc.network;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.systemticks.protobuf.rpc.data.MsgQueueElement;
import org.systemticks.protobuf.rpc.data.RpcServiceInterface;
import org.systemticks.protobuf.rpc.data.RpcServiceProxy;

public class NetworkBroker {

	private static NetworkBroker instance;
	private NetworkReceiver receiver;
	private NetworkSender sender;
	private BlockingQueue<MsgQueueElement> outQueue;
	private ServerSocket serverSocket;
	private Socket client;
	private Socket clientConnection;

	private NetworkBroker() {
		outQueue = new ArrayBlockingQueue<>(20);
		sender = new NetworkSender(	outQueue );
	}

	public static synchronized NetworkBroker INSTANCE () {
		if (NetworkBroker.instance == null) {
			NetworkBroker.instance = new NetworkBroker();
		}
		return NetworkBroker.instance;
	}

	private void bindStreams(DataInputStream in, DataOutputStream out) {
		receiver.bindStream(in);
		new Thread(receiver).start();
		sender.bindStream(out);
		new Thread(sender).start();
	}

	public BlockingQueue<MsgQueueElement> getOutgoingQueue() {
		return outQueue;
	}
	
	public void startServer(int port) throws IOException {

		serverSocket = new ServerSocket(port);
		System.out.println("Waiting for client...");
		client = serverSocket.accept();
		System.out.println("Client connected");
		
		DataInputStream inStream = new DataInputStream(new BufferedInputStream(client.getInputStream()));
		DataOutputStream outStream = new DataOutputStream(new BufferedOutputStream(client.getOutputStream()));
		
		receiver = new NetworkReceiver();
		
		bindStreams(inStream, outStream);
	}

	public void registerStub(RpcServiceInterface stub) {		
		if(serverSocket != null && !serverSocket.isClosed()) {			
			stub.bindIncomingQueue(receiver.registerMessageHandler(stub.getService()));			
			new Thread(stub).start();
		}		
	}
	
	
	public void connect(String host, int port) throws UnknownHostException, IOException {
		
		clientConnection = new Socket(host, port);
						
		DataOutputStream outStream = new DataOutputStream(new BufferedOutputStream(clientConnection.getOutputStream()));
		DataInputStream inStream = new DataInputStream(new BufferedInputStream(clientConnection.getInputStream()));
		
		receiver = new NetworkReceiverClient();
		
		bindStreams(inStream, outStream);				
	}
	
	
	public void disconnectClient() throws IOException {
		
		if(clientConnection != null && !clientConnection.isClosed()) {
			clientConnection.shutdownInput();
			clientConnection.shutdownOutput();
			clientConnection.close();
		}
		
	}
	
	public void registerProxy(RpcServiceProxy proxy) {		
		if(clientConnection != null && clientConnection.isConnected()) {			
			proxy.bindIncomingQueue(receiver.registerMessageHandler(proxy.getService(), proxy.getToken()));			
			new Thread(proxy).start();
		}		
	}
	
	
}
