package org.systemticks.protobuf.rpc.data;

import org.systemticks.protobuf.rpc.network.NetworkBroker;

public abstract class RpcServiceProxy extends RpcServiceInterface {

	private String token;
	
	public RpcServiceProxy(String service, String token) {
		super(service);
		this.token = token;
	}

		
	public String getToken() {
		return token;
	}


}
