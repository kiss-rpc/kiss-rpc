package org.systemticks.protobuf.rpc.data;

import java.util.concurrent.BlockingQueue;

import org.systemticks.protobuf.rpc.data.Header.MessageType;
import org.systemticks.protobuf.rpc.network.NetworkBroker;

import com.google.protobuf.InvalidProtocolBufferException;

public abstract class RpcServiceInterface implements Runnable {

	final protected BlockingQueue<MsgQueueElement> outGoingMessages;
	protected BlockingQueue<MsgQueueElement> incomingMessages;
	final protected String servicename;
	
	public RpcServiceInterface(String service) {
		outGoingMessages = NetworkBroker.INSTANCE().getOutgoingQueue();
		servicename = service;
	}
	
	public void bindIncomingQueue(BlockingQueue<MsgQueueElement> queue) {
		incomingMessages = queue;
	}
	
	public void enqueueNewMessage(String method, MessageType type, String client, byte[] payload) {
		
		Header header = Header.newBuilder().setType(type)
				.setSerivce(servicename)
				.setMethod(method)
				.setClientToken(client)
				.setPayloadSize(payload.length).build();

		outGoingMessages.add(new MsgQueueElement(header, payload));		
	}

	public String getService() {
		return servicename;
	}
	
	
	//dispatcher for incoming requests
	public void run() {
		
		while(true) {
			try {
				dispatchMessage(incomingMessages.take());
			} catch (InterruptedException | InvalidProtocolBufferException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	abstract public void dispatchMessage(MsgQueueElement take) throws InvalidProtocolBufferException;
	
	
}
