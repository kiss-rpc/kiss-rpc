package org.systemticks.protobuf.rpc.data;

public class MsgQueueElement {

	private Header header;
	private byte[] message;
		
	public MsgQueueElement(Header header, byte[] message) {
		super();
		this.header = header;
		this.message = message;
	}
	
	public Header getHeader() {
		return header;
	}
	public void setMethod(Header header) {
		this.header = header;
	}
	public byte[] getMessage() {
		return message;
	}
	public void setMessage(byte[] message) {
		this.message = message;
	}
	
}
